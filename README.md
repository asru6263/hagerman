# hagerman

Matlab/Octave code for estimation of processed S and N from mixture after Hagerman & Olofsson (2004), and of nonlinear error signal after Olofsson & Hansen (2006)

## Usage

These instructions assume that you have target signal s and noise signal n in your workspace. Both signals are NxM matrices, where N is the number of audio samples, and M is the number of channels. It is also assumed that you have a signal processing function

    y = process( x );

which can handle signals of the same dimension NxM. The processed signal y can have different dimensions than x.

Estimate processed S and N after Hagerman & Olofsson (2004):

    [sn1, sn2] = hagerman_generate_signals( s, n );
    psn1 = process( sn1 );
    psn2 = process( sn2 );
    [ps, pn] = hagerman_estimate( psn1, psn2 );

Estimate processed S and N after Hagerman & Olofsson (2004) and non-linearities after Olofsson & Hansen (2006):

    [sn1, sn2, sn3] = hagerman_generate_signals( s, n );
    psn1 = process( sn1 );
    psn2 = process( sn2 );
    psn3 = process( sn3 );
    [ps, pn, pe] = hagerman_estimate( psn1, psn2, psn3 );


## References

Hagerman, B., & Olofsson, Å. (2004).
*A method to measure the effect
of noise reduction algorithms using simultaneous speech and
noise.* Acta Acustica United with Acustica, 90(2), 356-361.

Olofsson, Å., & Hansen, M. (2006). *Objectively measured and
subjectively perceived distortion in nonlinear systems.* The Journal
of the Acoustical Society of America, 120(6),
3759–3769. [https://doi.org/10.1121/1.2372591](https://doi.org/10.1121/1.2372591)
