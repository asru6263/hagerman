addpath('..');
% generate test signals:
s = rand(1000,3)-0.5;
n = rand(1000,3)-0.5;
[sn1,sn2,sn3] = hagerman_generate_signals( s, n );
% test without processing:
[ps,pn,pe] = hagerman_estimate( sn1, sn2, sn3 );

expect_signal_difference_below( ps, s, 1e-10 );
expect_signal_difference_below( pn, n, 1e-10 );
expect_signal_difference_below( pe, 0, 1e-10 );

% test with processing:
n2 = rand(1000,3)-0.5;
psn1 = sn1+n2;
psn2 = sn2+n2;
psn3 = sn3+n2;
[ps,pn,pe] = hagerman_estimate( psn1, psn2, psn3 );

expect_signal_difference_below( rms(pe), 0.4, 0.05 );
