function expect_signal_difference_below( s_test, s_expect, maxdiff)
test_diff = rms( s_test-s_expect );
for k=1:numel(test_diff)
  if test_diff(k) > maxdiff
    error(sprintf(['signals differ in channel %d: rms(s_test-s_expect)=%g, ',...
                   'which is larger than maxdiff=%g'],...
                  k, test_diff(k), maxdiff) );
  end
end
